<?php

namespace CodeExampleDDDApi\Tests\Integration;

class UserEndpointsErrorsTest extends Endpoints
{

    public function testUserNotExists()
    {
        $this->remote("{$this->url}/user/100", 'GET', [], 404);
    }

    public function testCreateVoidUser()
    {
        $this->remote("{$this->url}/user", 'POST', [], 428);
    }

    public function testCreateUnderZeroUser()
    {
        $this->remote("{$this->url}/user", 'POST', ['age' => -1, 'name' => 'Under zero age user'], 412);
    }

    public function testSelectNotRangedUser()
    {
        $this->remote("{$this->url}/user", 'GET', ['filters' => ['age' => 1000]], 416);
    }

}
