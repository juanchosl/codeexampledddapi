<?php

namespace CodeExampleDDDApi\Tests\Unit;

use CodeExampleDDDApi\Context\Domain\User\ValueObject\FieldObject\UserName;
use CodeExampleDDDApi\Shared\Exception\PreconditionFailedException;

class UserNameTest extends \PHPUnit\Framework\TestCase
{

    public function testEmptyName()
    {
        try {
            new UserName("");
            $this->assertTrue(false);
        } catch (PreconditionFailedException $ex) {
            $this->assertTrue(true);
        } catch (\Exception $e) {
            $this->assertTrue(false);
        }
    }

}
