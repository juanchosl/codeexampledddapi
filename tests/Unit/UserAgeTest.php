<?php

namespace CodeExampleDDDApi\Tests\Unit;

use CodeExampleDDDApi\Context\Domain\User\ValueObject\FieldObject\UserAge;
use CodeExampleDDDApi\Shared\Exception\PreconditionFailedException;

class UserAgeTest extends \PHPUnit\Framework\TestCase
{

    public function testUnderZero()
    {
        try {
            new UserAge(-1);
            $this->assertTrue(false);
        } catch (PreconditionFailedException $ex) {
            $this->assertTrue(true);
        } catch (\Exception $e) {
            $this->assertTrue(false);
        }
    }

}
