<?php

namespace CodeExampleDDDApi\Tests\Functional;

use CodeExampleDDDApi\Context\Domain\User\UserService;
use CodeExampleDDDApi\Context\Domain\User\ValueObject\UserValueObject;
use CodeExampleDDDApi\Context\Domain\User\ValueObject\FieldObject\UserId;
use CodeExampleDDDApi\Context\Domain\User\ValueObject\FieldObject\UserName;
use CodeExampleDDDApi\Context\Domain\User\ValueObject\FieldObject\UserAge;
use CodeExampleDDDApi\Context\Infrastructure\User\Adapters\UserAdapter;
use CodeExampleDDDApi\Shared\Collection;

class UserDomainTest extends \PHPUnit\Framework\TestCase
{

    private function getContext(): UserService
    {
        return new UserService(new UserAdapter());
    }

    protected function find($id)
    {
        return $this->getContext()->getUserById(new UserId($id));
    }

    protected function delete($id)
    {
        return $this->getContext()->deleteUserById(new UserId($id));
    }

    public function testReadOneUser()
    {
        $element = $this->find('603bb3d819c20');
        $this->checkElement($element);
    }

    public function testReadSomeUsers()
    {
        $context = $this->getContext();
        $limits = [1, 2];
        foreach ($limits as $limit) {
            $response = $context->findUsersByCriterias([], $limit, 0);
            $this->assertInstanceOf(Collection::class, $response);
            $this->assertEquals($limit, $response->size());
            foreach ($response as $element) {
                $this->checkElement($element);
            }
        }
    }

    public function testReadSomeUsersUsingFilters()
    {
        $response = $this->getContext()->findUsersByCriterias(['age' => 20], 25, 0);
        $this->assertInstanceOf(Collection::class, $response);
        foreach ($response as $element) {
            $this->checkElement($element);
        }
    }

    public function testCreateAndUpdateAndDeleteAnUser()
    {
        $context = $this->getContext();
//        $element = $this->remote("{$this->url}/user", 'POST', ['name' => 'Creation name', 'age' => 40], 201);
        $user = new UserValueObject(new UserName("Creation Name"), new UserAge(40));
        $element = $context->createUser($user);
        $this->checkElement($element);

        $new_age = 30;
        $user = new UserValueObject(new UserName("Creation Name"), new UserAge(30));
        $element = $context->updateUserById($element->getUserId(), $user);
        $this->checkElement($element);
        $this->assertEquals($new_age, $element->getUserAge()->getValue());

        $this->delete($element->getUserId());
    }

    protected function checkElement($element)
    {
        $this->assertInstanceOf(UserValueObject::class, $element);
    }

}
