<?php

require __DIR__ . DIRECTORY_SEPARATOR . ".." . DIRECTORY_SEPARATOR . "src" . DIRECTORY_SEPARATOR . "bootstrap.php";

use Slim\Factory\AppFactory;
use Psr\Http\Message\ServerRequestInterface;
use CodeExampleDDDApi\Context\Infrastructure\User\Ports\Http;

$app = AppFactory::create();
$app->addRoutingMiddleware();
// Define Custom Error Handler
$customErrorHandler = function (
        ServerRequestInterface $request,
        \Throwable $exception,
        bool $displayErrorDetails,
        bool $logErrors,
        bool $logErrorDetails
) use ($app) {
    $response = $app->getResponseFactory()->createResponse();
    $response = $response->withStatus($code = ($exception->getCode() > 0 && $exception->getCode() <= 500) ? $exception->getCode() : 500);
    $response = $response->withHeader("Content-Type", "application/json");
    $response->getBody()->write(json_encode(['error' => $exception->getMessage()], JSON_UNESCAPED_UNICODE));

    return $response;
};

// Add Error Middleware
$errorMiddleware = $app->addErrorMiddleware(true, true, true);
$errorMiddleware->setDefaultErrorHandler($customErrorHandler);

$app->group('/user', function ($group) {
    $group->get('', Http\UserFindAction::class);
    $group->post('', Http\UserCreateAction::class);
    $group->patch('/{userid:[a-zA-Z0-9]+}', Http\UserUpdateAction::class);
    $group->get('/{userid:[a-zA-Z0-9]+}', Http\UserGetAction::class);
    $group->delete('/{userid:[a-zA-Z0-9]+}', Http\UserDeleteAction::class);
});

$app->run();
