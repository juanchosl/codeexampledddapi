<?php

use Illuminate\Database\Capsule\Manager as Capsule;

require __DIR__ . DIRECTORY_SEPARATOR . ".." . DIRECTORY_SEPARATOR . "vendor" . DIRECTORY_SEPARATOR . "autoload.php";

$dotenv = Dotenv\Dotenv::createImmutable(__DIR__ . DIRECTORY_SEPARATOR . ".." . DIRECTORY_SEPARATOR);
$dotenv->load();

defined('CONFIG_URL_DOMAIN') or define('CONFIG_URL_DOMAIN', $_ENV['WEBSERVER']);

$capsule = new Capsule;
$capsule->addConnection([
    "driver" => $_ENV['MYSQL_DRIVER'] ?? '',
    "host" => $_ENV['MYSQL_HOST'] ?? '',
    "database" => $_ENV['MYSQL_DATABASE'] ?? '',
    "username" => $_ENV['MYSQL_USER'] ?? '',
    "password" => $_ENV['MYSQL_PASSWORD'] ?? ''
]);
$capsule->setAsGlobal();
$capsule->bootEloquent();
