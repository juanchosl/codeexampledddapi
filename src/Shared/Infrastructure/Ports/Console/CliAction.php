<?php

namespace CodeExampleDDDApi\Shared\Infrastructure\Ports\Console;

class CliAction
{

    public function response($text, int $code = 0)
    {
        $text = json_encode($text, JSON_PRETTY_PRINT);
        echo $text;
        exit($code);
    }

}
