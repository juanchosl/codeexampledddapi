<?php

namespace CodeExampleDDDApi\Shared\Domain\ValueObject;

class DomainModelValueObject
{

    private $value;

    public function __construct($value)
    {
        $this->value = $value;
    }

    public function getValue()
    {
        return $this->value;
    }

    public function __toString()
    {
        return $this->getValue();
    }

    public function jsonSerialize()
    {
        return $this->getValue();
    }

}
