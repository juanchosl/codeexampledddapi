<?php

namespace CodeExampleDDDApi\Shared;

use CodeExampleDDDApi\Shared\Domain\ValueObject\DomainModelValueObject;

class Collection implements \Iterator, \Traversable, \JsonSerializable
{

    private $data = [];

    public function insert($object)
    {
        return array_push($this->data, $object);
    }

    public function current(): mixed
    {
        return current($this->data);
    }

    public function key(): mixed
    {
        return key($this->data);
    }

    public function next(): void
    {
        next($this->data);
    }

    public function rewind(): void
    {
        reset($this->data);
    }

    public function valid(): bool
    {
        return ($this->key() !== NULL && $this->key() !== FALSE);
    }

    public function size(): int
    {
        return count($this->data);
    }

    public function hasElements(): bool
    {
        return ($this->size() > 0);
    }

    public function jsonSerialize(): mixed
    {
        $response = [];
        foreach ($this->data as $data) {
            $response[] = json_decode(json_encode($data), false);
        }
        return $response;
    }

}
