<?php

namespace CodeExampleDDDApi\Shared\Exception;

class NotModifiedException extends \Exception
{

    const CODE = 304;
    const MESSAGE = "The selected element is not modified";

    public function __construct(string $message = self::MESSAGE)
    {
        parent::__construct($message, self::CODE, null);
    }

}
