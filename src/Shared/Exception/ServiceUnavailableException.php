<?php

namespace CodeExampleDDDApi\Shared\Exception;

class ServiceUnavailableException extends \Exception
{

    const CODE = 503;
    const MESSAGE = "The service is temporary unavailable, try again";

    public function __construct(string $message = self::MESSAGE)
    {
        parent::__construct($message, self::CODE, null);
    }

}
