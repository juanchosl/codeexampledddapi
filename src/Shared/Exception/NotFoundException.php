<?php

namespace CodeExampleDDDApi\Shared\Exception;

class NotFoundException extends \Exception
{

    const CODE = 404;
    const MESSAGE = "The selected element is not found";

    public function __construct(string $message = self::MESSAGE)
    {
        parent::__construct($message, self::CODE, null);
    }

}
