<?php

namespace CodeExampleDDDApi\Context\Application\User;

use CodeExampleDDDApi\Context\Domain\User\ValueObject\FieldObject\UserId;
use CodeExampleDDDApi\Context\Domain\User\ValueObject\UserValueObject;
use CodeExampleDDDApi\Shared\Exception\NotModifiedException;

class UserUpdateUseCase extends UserUseCase
{

    public function __invoke(UserId $user_id, UserValueObject $user): UserValueObject
    {
        $saved = $this->domain->updateUserById($user_id, $user);
        if (empty($saved)) {
            throw new NotModifiedException();
        }
        return $saved;
    }

}
