<?php

namespace CodeExampleDDDApi\Context\Application\User;

use CodeExampleDDDApi\Context\Domain\User\ValueObject\UserValueObject;
use CodeExampleDDDApi\Shared\Exception\ServiceUnavailableException;

class UserCreateUseCase extends UserUseCase
{

    public function __invoke(UserValueObject $user): UserValueObject
    {
        $saved = $this->domain->createUser($user);
        if (empty($saved)) {
            throw new ServiceUnavailableException();
        }
        return $saved;
    }

}
