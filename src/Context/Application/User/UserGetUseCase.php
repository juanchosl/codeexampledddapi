<?php

namespace CodeExampleDDDApi\Context\Application\User;

use CodeExampleDDDApi\Context\Domain\User\ValueObject\FieldObject\UserId;
use CodeExampleDDDApi\Shared\Exception\NotFoundException;

class UserGetUseCase extends UserUseCase
{

    public function __invoke(UserId $user_id)
    {
        $user = $this->domain->getUserById($user_id);
        if (empty($user)) {
            throw new NotFoundException();
        }
        return $user;
    }

}
