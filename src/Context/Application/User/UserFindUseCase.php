<?php

namespace CodeExampleDDDApi\Context\Application\User;

use CodeExampleDDDApi\Shared\Exception\RangeNotSatisfiableException;

class UserFindUseCase extends UserUseCase
{

    public function __invoke(array $criterias, int $take, int $skip)
    {

        $collection = $this->domain->findUsersByCriterias($criterias, $take, $skip);
        if (!$collection->hasElements()) {
            throw new RangeNotSatisfiableException();
        }
        return $collection;
    }

}
