<?php

namespace CodeExampleDDDApi\Context\Application\User;

use CodeExampleDDDApi\Context\Domain\User\ValueObject\FieldObject\UserId;
use CodeExampleDDDApi\Shared\Exception\NotModifiedException;

class UserDeleteUseCase extends UserUseCase
{

    public function __invoke(UserId $user_id)
    {
        $deleted = $this->domain->deleteUserById($user_id);
        if (!$deleted) {
            throw new NotModifiedException();
        }
        return true;
    }

}
