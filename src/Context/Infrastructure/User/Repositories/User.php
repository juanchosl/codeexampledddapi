<?php

namespace CodeExampleDDDApi\Context\Infrastructure\User\Repositories;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{

    protected $casts = [
        'id' => 'string',
        'age' => 'integer'
    ];
    protected $hidden = [
        'created_at',
        'updated_at'
    ];

    public function save($options = [])
    {
        $id = $this->id;
        $save = parent::save($options);
        $this->id = $id;
        return $save;
    }

}
