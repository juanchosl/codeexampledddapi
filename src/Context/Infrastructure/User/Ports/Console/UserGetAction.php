<?php

namespace CodeExampleDDDApi\Context\Infrastructure\User\Ports\Console;


class UserGetAction extends UserAction
{

    public function __invoke($user_id)
    {
        $user = $this->getHandler()->getUser($user_id);
        return $this->response($user);
    }

}
