<?php

namespace CodeExampleDDDApi\Context\Infrastructure\User\Ports\Console;

use CodeExampleDDDApi\Shared\Exception\PreconditionRequiredException;

class UserUpdateAction extends UserAction
{

    public function __invoke($user_id, $args)
    {
        $body = $args;
        if (empty($body)) {
            throw new PreconditionRequiredException("You need to send the fields to update");
        }

        $saved = $this->getHandler()->updateUser($user_id, $body);
        return $this->response($saved);
    }

}
