<?php

namespace CodeExampleDDDApi\Context\Infrastructure\User\Ports\Console;

class UserFindAction extends UserAction
{

    public function __invoke(array $query_filters, int $offset = 15, int $page = 1)
    {
        $filters = [];
        foreach ($query_filters as $field => $value) {
            if (in_array($field, ['age', 'name'])) {
                $filters[] = [$field, '=', $value];
            }
        }
        $take = max(1, $offset);
        $skip = (max(1, $page) - 1) * $take;

        $users = $this->getHandler()->getUsers($filters, $take, $skip);
        return $this->response($users);
    }

}
