<?php

namespace CodeExampleDDDApi\Context\Infrastructure\User\Ports\Console;

use CodeExampleDDDApi\Shared\Exception\PreconditionRequiredException;

class UserCreateAction extends UserAction
{

    public function __invoke(array $args)
    {
        $body = $args;
        if (empty($body)) {
            throw new PreconditionRequiredException("You need to send the fields to insert");
        }

        $saved = $this->getHandler()->createUser($body);
        return $this->response($saved, 201);
    }

}
