<?php

namespace CodeExampleDDDApi\Context\Infrastructure\User\Ports\Console;

use CodeExampleDDDApi\Context\Infrastructure\User\Handlers\UserHandler;

class UserAction extends \CodeExampleDDDApi\Shared\Infrastructure\Ports\Console\CliAction
{

    public function getHandler()
    {
        return new UserHandler();
    }

}
