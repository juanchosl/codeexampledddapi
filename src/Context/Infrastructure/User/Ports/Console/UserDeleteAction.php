<?php

namespace CodeExampleDDDApi\Context\Infrastructure\User\Ports\Console;

class UserDeleteAction extends UserAction
{

    public function __invoke($user_id)
    {
        $this->getHandler()->deleteUser($user_id);
        return $this->response("deleted", 205);
    }

}
