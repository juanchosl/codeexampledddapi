<?php

namespace CodeExampleDDDApi\Context\Infrastructure\User\Ports\Http;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use CodeExampleDDDApi\Context\Application\User\UserFindUseCase;
use CodeExampleDDDApi\Shared\Exception\RangeNotSatisfiableException;

class UserFindAction extends UserAction
{

    public function __invoke(Request $request, Response $response, array $args)
    {
        $query_filters = $request->getQueryParam('filters', []);
        $filters = [];
        foreach ($query_filters as $field => $value) {
            if (in_array($field, ['age', 'name'])) {
                $filters[] = [$field, '=', $value];
            }
        }
        $take = max(1, $request->getQueryParam('offset', 15));
        $skip = (max(1, $request->getQueryParam('page', 1)) - 1) * $take;

//        $body = call_user_func(new UserFindUseCase(), $filters, $take, $skip);
        $users = $this->getHandler()->getUsers($filters, $take, $skip);
        return $this->response($response, $users);
    }

}
