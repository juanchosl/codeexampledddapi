<?php

namespace CodeExampleDDDApi\Context\Infrastructure\User\Ports\Http;

use CodeExampleDDDApi\Context\Infrastructure\User\Handlers\UserHandler;

class UserAction extends \CodeExampleDDDApi\Shared\Infrastructure\Ports\Http\ApiAction
{

    public function getHandler()
    {
        return new UserHandler();
    }

}
