<?php

namespace CodeExampleDDDApi\Context\Infrastructure\User\Ports\Http;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use CodeExampleDDDApi\Shared\Exception\PreconditionRequiredException;

class UserUpdateAction extends UserAction
{

    public function __invoke(Request $request, Response $response, array $args)
    {
        $body = $request->getParsedBody();
        if (empty($body)) {
            throw new PreconditionRequiredException("You need to send the fields to update");
        }
        $saved = $this->getHandler()->updateUser($args['userid'], $body);
        return $this->response($response, $saved);
    }

}
