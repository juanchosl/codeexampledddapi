<?php

namespace CodeExampleDDDApi\Context\Infrastructure\User\Ports\Http;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

class UserGetAction extends UserAction
{

    public function __invoke(Request $request, Response $response, array $args)
    {
        $user = $this->getHandler()->getUser($args['userid']);
        return $this->response($response, $user);
    }

}
