<?php

namespace CodeExampleDDDApi\Context\Infrastructure\User\Ports\Http;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

class UserDeleteAction extends UserAction
{

    public function __invoke(Request $request, Response $response, array $args)
    {

        $this->getHandler()->deleteUser($args['userid']);
        return $this->response($response, null, 205);
    }

}
