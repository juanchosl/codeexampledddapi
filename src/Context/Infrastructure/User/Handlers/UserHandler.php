<?php

namespace CodeExampleDDDApi\Context\Infrastructure\User\Handlers;

use CodeExampleDDDApi\Context\Application\User\UserGetUseCase;
use CodeExampleDDDApi\Context\Domain\User\ValueObject\UserValueObject;
use CodeExampleDDDApi\Context\Domain\User\ValueObject\FieldObject\UserId;
use CodeExampleDDDApi\Context\Domain\User\ValueObject\FieldObject\UserAge;
use CodeExampleDDDApi\Context\Domain\User\ValueObject\FieldObject\UserName;
use CodeExampleDDDApi\Context\Domain\User\UserService;
use CodeExampleDDDApi\Context\Infrastructure\User\Adapters\UserAdapter;
use CodeExampleDDDApi\Context\Application\User\UserFindUseCase;
use CodeExampleDDDApi\Context\Application\User\UserUpdateUseCase;
use CodeExampleDDDApi\Context\Application\User\UserCreateUseCase;
use CodeExampleDDDApi\Context\Application\User\UserDeleteUseCase;

class UserHandler
{

    protected $context;

    public function __construct()
    {
        $this->context = new UserService(new UserAdapter());
    }

    protected function getContext()
    {
        return $this->context;
    }

    public function createUser(array $body)
    {
        $user = new UserValueObject(
                new UserName($body['name']),
                new UserAge($body['age'])
        );
        return call_user_func(new UserCreateUseCase($this->getContext()), $user);
    }

    public function updateUser($user_id, array $body)
    {
        $user_original_values = $this->getUser($user_id);
        $user = new UserValueObject(
                new UserName($body['name'] ?? $user_original_values->getUserName()->getValue()),
                new UserAge($body['age'] ?? $user_original_values->getUserAge()->getValue()),
                new UserId($user_id)
        );
        return call_user_func(new UserUpdateUseCase($this->getContext()), new UserId($user_id), $user);
    }

    public function getUsers(array $filters = [], int $take = 15, int $skip = 0)
    {
        return call_user_func(new UserFindUseCase($this->getContext()), $filters, $take, $skip);
    }

    public function deleteUser($user_id)
    {
        return call_user_func(new UserDeleteUseCase($this->getContext()), new UserId($user_id));
    }

    public function getUser($user_id)
    {
        return call_user_func(new UserGetUseCase($this->getContext()), new UserId($user_id));
    }

}
