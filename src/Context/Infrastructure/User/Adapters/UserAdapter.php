<?php

namespace CodeExampleDDDApi\Context\Infrastructure\User\Adapters;

use CodeExampleDDDApi\Context\Infrastructure\User\Repositories\User;
use CodeExampleDDDApi\Context\Domain\User\ValueObject\UserValueObject;
use CodeExampleDDDApi\Context\Domain\User\ValueObject\FieldObject\UserName;
use CodeExampleDDDApi\Context\Domain\User\ValueObject\FieldObject\UserAge;
use CodeExampleDDDApi\Context\Domain\User\ValueObject\FieldObject\UserId;
use CodeExampleDDDApi\Context\Domain\User\UsersCollection;
use CodeExampleDDDApi\Shared\Collection;
use CodeExampleDDDApi\Shared\Exception\NotFoundException;
use CodeExampleDDDApi\Shared\Exception\NotModifiedException;

class UserAdapter implements \CodeExampleDDDApi\Context\Domain\User\Contract\UserRepositoryContract
{

    private $repository;

    public function __construct()
    {
        $this->repository = new User;
    }

    public function delete(UserId $id): bool
    {
        if (empty($object = $this->repository->find($id->getValue()))) {
            throw new NotFoundException();
        }
        if (!$object->delete()) {
            throw new NotModifiedException();
        }
        return true;
    }

    public function find(UserId $id): UserValueObject
    {
        $user = $this->repository->find($id->getValue());
        if (empty($user)) {
            throw new NotFoundException();
        }

        return new UserValueObject(
                new UserName($user->name),
                new UserAge($user->age),
                new UserId($user->id)
        );
    }

    public function save(UserValueObject $user): UserValueObject
    {
        $this->repository->id = $user->getUserId()->getValue();
        $this->repository->name = $user->getUserName()->getValue();
        $this->repository->age = $user->getUserAge()->getValue();
        if (!$this->repository->save()) {
            throw new NotModifiedException();
        }
        return new UserValueObject(
                new UserName($this->repository->name),
                new UserAge($this->repository->age),
                new UserId($this->repository->id)
        );
    }

    public function search(array $filters, int $take = 15, int $skip = 0): UsersCollection
    {

        $users = $this->repository->where($filters)
                ->take($take)
                ->skip($skip)
                ->get();
        $results = new UsersCollection();
        foreach ($users as $user) {
            $user_obj = new UserValueObject(
                    new UserName($user->name),
                    new UserAge($user->age),
                    new UserId($user->id)
            );
            $results->insert($user_obj);
        }
        return $results;
    }

    public function update(UserId $id, UserValueObject $user): UserValueObject
    {
        try {
            $object = $this->repository->findOrFail($id->getValue());
        } catch (\Exception $ex) {
            throw new NotFoundException();
        }
        $object->name = $user->getUserName()->getValue();
        $object->age = $user->getUserAge()->getValue();
        if (!$object->save()) {
            throw new NotModifiedException();
        }
        return new UserValueObject(
                new UserName($object->name),
                new UserAge($object->age),
                new UserId($object->id)
        );
    }

}
