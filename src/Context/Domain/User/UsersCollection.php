<?php

namespace CodeExampleDDDApi\Context\Domain\User;

use CodeExampleDDDApi\Shared\Collection;
use CodeExampleDDDApi\Context\Domain\User\ValueObject\UserValueObject;

class UsersCollection extends Collection
{

    public function insert($object)
    {
        return parent::insert($object);
    }

}
