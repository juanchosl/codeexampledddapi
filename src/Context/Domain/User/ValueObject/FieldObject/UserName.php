<?php

namespace CodeExampleDDDApi\Context\Domain\User\ValueObject\FieldObject;

use CodeExampleDDDApi\Shared\Domain\ValueObject\DomainModelValueObject;
use CodeExampleDDDApi\Shared\Exception\PreconditionFailedException;

class UserName extends DomainModelValueObject
{

    public function __construct(string $value)
    {
        if (empty($value)) {
            throw new PreconditionFailedException("The name can not be empty");
        }
        parent::__construct($value);
    }

}
