<?php

namespace CodeExampleDDDApi\Context\Domain\User\ValueObject\FieldObject;

use CodeExampleDDDApi\Shared\Domain\ValueObject\DomainModelValueObject;
use CodeExampleDDDApi\Shared\Exception\PreconditionFailedException;

class UserAge extends DomainModelValueObject
{

    public function __construct(int $value)
    {
        if ($value < 0) {
            throw new PreconditionFailedException("The age can not be less of 0");
        }
        parent::__construct($value);
    }

}
