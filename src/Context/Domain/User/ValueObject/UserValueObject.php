<?php

namespace CodeExampleDDDApi\Context\Domain\User\ValueObject;

use CodeExampleDDDApi\Shared\Domain\ValueObject\DomainModelValueObject;
use CodeExampleDDDApi\Context\Domain\User\ValueObject\FieldObject\UserId;
use CodeExampleDDDApi\Context\Domain\User\ValueObject\FieldObject\UserName;
use CodeExampleDDDApi\Context\Domain\User\ValueObject\FieldObject\UserAge;

class UserValueObject implements \JsonSerializable
{

    private $user_name;
    private $user_age;
    private $user_id;

    public function __construct(
            UserName $user_name,
            UserAge $user_age,
            UserId $user_id = null
    )
    {
        $this->user_name = $user_name;
        $this->user_age = $user_age;
        $this->user_id = $user_id ?? new UserId(uniqid());
    }

    public function getUserName(): UserName
    {
        return $this->user_name;
    }

    public function getUserAge(): UserAge
    {
        return $this->user_age;
    }

    public function getUserId(): UserId
    {
        return $this->user_id;
    }

    public function jsonSerialize(): mixed
    {
        return [
            'id' => $this->getUserId()->getValue(),
            'name' => $this->getUserName()->getValue(),
            'age' => $this->getUserAge()->getValue()
        ];
    }

}
