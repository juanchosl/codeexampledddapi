<?php

namespace CodeExampleDDDApi\Context\Domain\User;

use CodeExampleDDDApi\Context\Domain\User\ValueObject\UserValueObject;
use CodeExampleDDDApi\Context\Domain\User\ValueObject\FieldObject\UserId;
use CodeExampleDDDApi\Context\Domain\User\UsersCollection;
use CodeExampleDDDApi\Shared\Collection;

class UserService
{

    protected $repository;

    public function __construct(Contract\UserRepositoryContract $repository)
    {
        $this->repository = $repository;
    }

    public function createUser(UserValueObject $user): UserValueObject
    {
        return $this->repository->save($user);
    }

    public function getUserById(UserId $user_id): UserValueObject
    {
        return $this->repository->find($user_id);
    }

    public function deleteUserById(UserId $user_id): bool
    {
        return $this->repository->delete($user_id);
    }

    public function updateUserById(UserId $user_id, UserValueObject $user): UserValueObject
    {
        return $this->repository->update($user_id, $user);
    }

    public function findUsersByCriterias($criterias, $take, $skip): UsersCollection
    {
        return $this->repository->search($criterias, $take, $skip);
    }

}
