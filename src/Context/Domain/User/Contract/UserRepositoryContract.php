<?php

namespace CodeExampleDDDApi\Context\Domain\User\Contract;

use CodeExampleDDDApi\Context\Domain\User\ValueObject\FieldObject\UserId;
use CodeExampleDDDApi\Context\Domain\User\ValueObject\UserValueObject;

interface UserRepositoryContract
{

    public function find(UserId $id);

    public function delete(UserId $id);

    public function update(UserId $id, UserValueObject $user);

    public function save(UserValueObject $user);

    public function search(array $criterias, int $take, int $skip);
}
